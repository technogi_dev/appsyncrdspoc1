import { expect as expectCDK, matchTemplate, MatchStyle } from '@aws-cdk/assert';
import * as cdk from '@aws-cdk/core';
import * as Appsynctest7 from '../lib/appsynctest7-stack';

test('Empty Stack', () => {
    const app = new cdk.App();
    // WHEN
    const stack = new Appsynctest7.Appsynctest7Stack(app, 'MyTestStack');
    // THEN
    expectCDK(stack).to(matchTemplate({
      "Resources": {}
    }, MatchStyle.EXACT))
});
