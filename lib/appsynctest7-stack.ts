import * as cdk from '@aws-cdk/core';
import * as ec2 from '@aws-cdk/aws-ec2';
import * as lambda from '@aws-cdk/aws-lambda';
import * as rds from '@aws-cdk/aws-rds';
import * as appsync from '@aws-cdk/aws-appsync';
export class Appsynctest7Stack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const api = new appsync.GraphqlApi(this, 'Api7',{
      name: 'test7',
      schema: appsync.Schema.fromAsset('graphql/schema.graphql'),
      authorizationConfig:{
        defaultAuthorization:{
          authorizationType: appsync.AuthorizationType.API_KEY,
          apiKeyConfig:{
            expires: cdk.Expiration.after(cdk.Duration.days(5))
          }
        }
      }
    })

    const vpc = new ec2.Vpc(this,'apptest7Vpc')
 
    const cluster = new rds.ServerlessCluster(this,'Aurora7Cluster',{
      engine: rds.DatabaseClusterEngine.AURORA_POSTGRESQL,
      parameterGroup: rds.ParameterGroup.fromParameterGroupName(this, 'ParameterGroup', 'default.aurora-postgresql10'),
      defaultDatabaseName: 'Test7DB',
      vpc,
      scaling: { autoPause: cdk.Duration.seconds(0) }
    })
 
    const postFn = new lambda.Function(this, 'MyFunction', {
      runtime: lambda.Runtime.NODEJS_14_X,
      code: new lambda.AssetCode('lambda-fns'),
      handler: 'index.handler',
      memorySize: 1024,
      environment: {
        CLUSTER_ARN: cluster.clusterArn,
        SECRET_ARN: cluster.secret?.secretArn || '',
        DB_NAME: 'Test7DB',
        AWS_NODEJS_CONNECTION_REUSE_ENABLED: '1'
      },
    });
    
     // Grant access to the cluster from the Lambda function
     cluster.grantDataApiAccess(postFn);
     // Set the new Lambda function as a data source for the AppSync API
     const lambdaDs = api.addLambdaDataSource('lambdaDatasource', postFn);
// 
    lambdaDs.createResolver({
      typeName: 'Query',
      fieldName: 'listPosts',
    });
    lambdaDs.createResolver({
      typeName: 'Query',
      fieldName: 'getPostById'
    });
    lambdaDs.createResolver({
      typeName: 'Mutation',
      fieldName: 'createPost'
    });
    lambdaDs.createResolver({
      typeName: 'Mutation',
      fieldName: 'updatePost'
    });
    lambdaDs.createResolver({
      typeName: 'Mutation',
      fieldName: 'deletePost'
    });

    new cdk.CfnOutput(this, 'AppSyncAPIURL', {
      value: api.graphqlUrl
    });
    new cdk.CfnOutput(this, 'AppSyncAPIKey', {
      value: api.apiKey || ''
    });
    new cdk.CfnOutput(this, 'ProjectRegion', {
      value: this.region
    });
  }
}
